Multi-column layout for works on Archive of our Own (AO3).

Sample screenshot:

![Text in two columns](https://gitlab.com/keturn/multicolumn-ao3-usercss/raw/master/screenshot.webp "Screenshot")

(Sample text is [_The Internet Is Not Just For Porn_](https://archiveofourown.org/works/304382) by cyerus, as viewed in Firefox.)

I recommend [Stylus](https://add0n.com/stylus.html) to apply user styles.
